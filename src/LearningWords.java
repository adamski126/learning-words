import jdk.nashorn.internal.ir.WhileNode;

import java.util.*;

class LearningWords {

    List<String> wordsToLearn = new ArrayList<>();

    //fill list with all avaible words to learn
    void getWords() throws Exception {
        wordsToLearn = URLReader.readWords("http://szgrabowski.iis.p.lodz.pl/zpo18/1500.txt");
    }

    void learningProcess(){
        //CONFIG
        int n = 10; //for how many days we should learn words
        int k = 3; //after k days we will forget 0-2 words
        double p = 0.5; //change to forget the word

        Map<String, Integer> words = new HashMap<>(); //map with learned words
        Set<String> learnedWords = new HashSet<>(); //additional set to know which words we arleardy know
        List<String> newWords = new ArrayList<>(); //additional list to print new learned words every day
        List<String> forgottenWords = new ArrayList<>(); //additional list to print forgotten words every day

        Random random = new Random();

        for(int i=0; i<n; i++){

            System.out.println("Day " + (i+1));

            //adding +1 to words map value (+1 day)
            if(!words.isEmpty()){
                words.forEach((key,val)->{
                   words.computeIfPresent(key,(ke,va)-> va+1);
                });
            }


            int howManyToday = 2;//how many words we will learn each day (2)

            //add 2 words to map (learning)
            int j=0;
            while (j<howManyToday){
                String randomWord = wordsToLearn.get(random.nextInt(wordsToLearn.size())); //get one random word
                if(learnedWords.add(randomWord)){ //if word was added to set
                    words.put(randomWord,0);
                    newWords.add(randomWord);
                    j++;
                }
            }

            //forgetting words
            if(!words.isEmpty()){

                int forgottenWordsCount = 0;
                int wordsToForget = random.nextInt(3);

                //setting words to remove
                for(Map.Entry<String, Integer> entry : words.entrySet()){
                    if(forgottenWordsCount<wordsToForget) {
                        if (entry.getValue() >= k) {
                            Double r = random.nextDouble();

                            if (r < p) {//if r > p then forget the word
                                words.computeIfPresent(entry.getKey(), (ke,va)-> va = -10);
                                learnedWords.remove(entry.getKey()); //removing word from set
                                forgottenWordsCount++;
                            }
                        }
                    }
                }

                //adding forgotten words to forgotten words array list
                words.forEach((key,val)->{
                    if(val == -10){
                        forgottenWords.add(key);
                    }
                });

                //removing worlds from map
                words.values().removeAll(Collections.singleton(-10));
            }


            //print map with learned words
            if(!words.isEmpty()){
                System.out.println("New words:\t\t\t" + newWords.get(0) + " " + newWords.get(1)); //print new worlds
                newWords.clear(); //clear new words array list
                if(forgottenWords.isEmpty()){
                    System.out.println("Forgotten words:\t ---");
                } else {
                    System.out.print("Forgotten words:\t");
                    for(int t=0; t<forgottenWords.size(); t++){
                        System.out.print(forgottenWords.get(t) + " ");
                    }
                    System.out.println();
                    forgottenWords.clear(); //clear forgotten words list
                }
                System.out.println(words.keySet()+"\n");
            }
        }
    }
}
