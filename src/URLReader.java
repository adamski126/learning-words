import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

public class URLReader {

    public static ArrayList<String> readWords(String URL) throws Exception {

        ArrayList<String> words = new ArrayList<>();

        java.net.URL oracle = new URL(URL);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(oracle.openStream()));

        String inputLine;
        while ((inputLine = in.readLine()) != null)
            words.add(inputLine);
        in.close();

        return words;
    }

}
